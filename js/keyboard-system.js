import {page} from './router.js'

const KeyboardSystem = class extends HTMLElement {
    connectedCallback() {
	document.addEventListener('keydown', this.handleKeyboard, false);
    }
    handleKeyboard(event) {
	switch (event.key) {
	    case 'Escape':
		page('/')
		break;
	}
    }
}

export default KeyboardSystem
