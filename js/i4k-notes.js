import './storage.js'
import { buildLink } from './router.js'
import ApplicationRouter from './application-router.js'
import KeyboardSystem from './keyboard-system.js'

customElements.define('application-router', ApplicationRouter)
customElements.define('keyboard-system', KeyboardSystem)

const I4kNotes = class extends HTMLElement {
    connectedCallback() {
	console.log('i4k-notes!')
	this.getAttributes()
	this.render()
    }
    getAttributes() {
	// config
	this.debug = this.getAttribute("debug") ? true : false
	this.debugStorage = this.getAttribute("debug-storage") ? true : false
	this.accessClaimDirectory = this.getAttribute("access-claim-directory")
	this.apiKeyDropbrox = this.getAttribute('api-key-dropbrox')
	this.apiKeyGoogledrive = this.getAttribute('api-key-googledrive')
    }
    
    render() {
	let $rsStorage = document.createElement('rs-storage')
	if (this.debugStorage) $rsStorage.setAttribute('debug', true)
	$rsStorage.setAttribute('hidden', true)
	$rsStorage.setAttribute('access-claim-directory', this.accessClaimDirectory)
	$rsStorage.setAttribute('access-claim-rights', 'rw')
	$rsStorage.setAttribute('caching-enable-directory', `/${this.accessClaimDirectory}/`)
	$rsStorage.setAttribute('api-key-dropbrox', this.apiKeyDropbrox)
	$rsStorage.setAttribute('api-key-googledrive', this.apiKeyGoogledrive)
	$rsStorage.setAttribute('listen-connected', true)
	$rsStorage.setAttribute('listen-disconnected', true)
	$rsStorage.setAttribute('listen-network-online', true)
	$rsStorage.setAttribute('listen-network-offline', true)

	let $rsWidgetStatus = document.createElement('rs-widget-status')
	let $rsWidgetLink = buildLink({
	    href: '/account',
	    text: 'sync'
	})
	$rsWidgetStatus.append($rsWidgetLink)

	let $notificationSystem = document.createElement('notification-system')
	$notificationSystem.setAttribute('position', 'bottom-right')

	let $keyboardSystem = document.createElement('keyboard-system')

	let $applicationRouter = document.createElement('application-router')

	this.append($rsStorage)
	this.append($rsWidgetStatus)
	this.append($notificationSystem)	
	this.append($keyboardSystem)
	this.append($applicationRouter)
    }
}

export default I4kNotes
