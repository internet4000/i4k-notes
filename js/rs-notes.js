import RsNoteCard from './rs-note-card.js'
import {buildLink} from './router.js'

customElements.define('rs-note-card', RsNoteCard)

const RsNotes = class extends HTMLElement {
    notes = []
    
    constructor() {
	super()
    }

    async connectedCallback() {
	this.setupRS()
	await this.setupView()
    }

    async setupView() {
	this.setAttribute('loading', true)
	await this.loadNotes()
	this.removeAttribute('loading')
	
	this.setAttribute('notes', this.notes.length)

	this.render()
    }

    setupRS() {
	this.remoteStorageDOM = document.querySelector('rs-storage')

	if (!this.remoteStorageDOM) {
	    console.error('Missing <rs-storage> element')
	    return
	} 

	this.remoteStorage = this.remoteStorageDOM.remoteStorage
    }
    
    loadNotes = async () => {
	this.notes = await this.remoteStorage.note4000.getAll()
    }

    handleDelete = async () => {
	console.log('handleDelete')
	await this.setupView()
    }
    
    render() {
	this.innerHTML = ''

	if (!this.notes.length) {
	    let $noNote = buildLink({
		title: 'Click to create a new note!',
		text: 'Start taking notes',
		href: '/note'
	    })
	    this.appendChild($noNote)
	} else {
	    let $notesHeader = document.createElement('rs-notes-header')
	    $notesHeader.innerText = `${this.notes.length} notes`
	    this.appendChild($notesHeader)
	    
	    this.notes.forEach(note => {
		let $noteCard = document.createElement('rs-note-card')
		$noteCard.setAttribute('note-id', note.id)
		$noteCard.addEventListener('noteDeleted', this.handleDelete, false)
		if (note.data) {
		    $noteCard.setAttribute('note-data', note.data)
		}
		this.appendChild($noteCard)
	    })
	}
    }
}

export default RsNotes
