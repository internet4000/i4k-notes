import {buildLink} from './router.js'

const NavSite = class extends HTMLElement {
    constructor() {
	super()
    }

    connectedCallback() {
	this.render()
    }

    render() {
	let $nav = document.createElement('nav')
	let $navLinkNewNote = buildLink({
	    text: 'notes',
	    href: '/',
	    title: 'Go back to all notes'
	})

	$nav.append($navLinkNewNote)
	this.append($nav)
    }
}

export default NavSite
