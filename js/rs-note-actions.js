import {page} from './router.js'

const RsNoteActions = class extends HTMLElement {
    constructor() {
	super()
    }

    async connectedCallback() {
	this.getAttributes()
	this.setupRS()
	this.render()
    }

    getAttributes() {
	// config
	this.debug = this.getAttribute('debug') ? true : false
	this.noteId = this.getAttribute('note-id') || null
    }

    setupRS() {
	this.remoteStorageDOM = document.querySelector('rs-storage')

	if (!this.remoteStorageDOM) {
	    console.error('Missing <rs-storage> element')
	    return
	}

	this.remoteStorage = this.remoteStorageDOM.remoteStorage
    }

    render() {
	
	let $noteId = document.createElement('code')
	$noteId.innerText = this.noteId
	$noteId.setAttribute('title', 'note unique ID')
	
	const $noteDelete = document.createElement('button')
	$noteDelete.onclick = this.deleteNote
	$noteDelete.innerText = 'delete'
	$noteDelete.setAttribute('title', 'delete this note')

	this.append($noteId)
	this.append($noteDelete)
    }

    deleteNote = async () => {
	const {
	    remoteStorage,
	    noteId
	} = this

	try {
	    await remoteStorage.note4000.delete(noteId)
	} catch (error) {
	    console.log('Error deleting note')
	}
	
	const event = new CustomEvent('noteDeleted', {
	    bubbles: true
	})

	this.dispatchEvent(event)
    }
}

export default RsNoteActions
