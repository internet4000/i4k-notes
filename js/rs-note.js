import {buildLink} from './router.js'
import {notify} from './notifications.js'
import {page} from './router.js'

const RsNote = class extends HTMLElement {
    constructor() {
	super()
    }
    async connectedCallback() {
	this.getAttributes()
	this.setupRS()
	await this.loadNote()

	/* save initial note when empty, in any case.
	   we will delete on exit if empty */
	if (!this.content) {
	    this.remoteStorage.note4000.storeNote({
		id: this.noteId,
		content: ''
	    })
	}

	this.render()
    }
    async disconnectedCallback() {
	if (this.getAttribute('deleted')) return

	if (this.content) {
	    await this.remoteStorage.note4000.storeNote({
		id: this.noteId,
		content: this.content
	    })
	    notify(`${this.noteId} saved`)
	} else {
	    await this.remoteStorage.note4000.delete(this.noteId)
	}
    }
    getAttributes() {
	// config
	this.debug = this.getAttribute('debug') ? true : false
	this.noteId = this.getAttribute('note-id') || null
    }
    setupRS() {
	this.remoteStorageDOM = document.querySelector('rs-storage')	

	if (!this.remoteStorageDOM) {
	    console.error('Missing <rs-storage> element')
	    return
	} 

	this.remoteStorage = this.remoteStorageDOM.remoteStorage
    }
    loadNote = async () => {
	this.note = await this.remoteStorage.note4000.get(this.noteId)
	this.content = this.note.data || ''
    }

    handleChange = event => {
	event.preventDefault()
	// set the state to reflect on the UI
	// and save content to reflect the data on storage server
	this.content = event.target.value
	const remoteStorage = this.remoteStorage
	const id = this.noteId

	remoteStorage.note4000.storeNote({
	    id: id,
	    content: event.target.value
	})
    }

    handleDelete = () => {
	this.setAttribute('deleted', true)
	page('/')
    }
    
    render() {
	let $noteHeader = document.createElement('header')

	let $linkHome = buildLink({
	    text: '←',
	    href: '/',
	    title: 'Save and navigate to the Homepage'
	})

	const $noteActions = document.createElement('rs-note-actions')
	$noteActions.setAttribute('note-id', this.noteId)
	$noteActions.addEventListener('noteDeleted', this.handleDelete, false)

	let $noteData = document.createElement('textarea')
	$noteData.placeholder = '...'
	$noteData.value = this.content
	$noteData.oninput = this.handleChange

	$noteHeader.append($linkHome)
	$noteHeader.append($noteActions)
	this.append($noteHeader)
	this.append($noteData)

	$noteData.focus()
    }
}

export default RsNote
