import {
    RsStorage,
    RsWidget,
    RsWidgetStatus,
} from 'https://cdn.skypack.dev/remote-storage-elements'

import notesModule from './storage-modules/notes.js'


class NotesStorage extends RsStorage {
    storageModule = notesModule
}

customElements.define('rs-storage', NotesStorage)
customElements.define('rs-widget', RsWidget)
customElements.define('rs-widget-status', RsWidgetStatus)

export default {
    NotesStorage,
    RsWidget
}
