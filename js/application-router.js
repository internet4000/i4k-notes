import { page } from "./router.js";

import NavSite from "./nav-site.js";
import RsNotes from "./rs-notes.js";
import RsNote from "./rs-note.js";
import RsNoteActions from "./rs-note-actions.js";
import RsNoteNew from "./rs-note-new.js";
import RsAccount from "./rs-account.js";

customElements.define("nav-site", NavSite);
customElements.define("rs-notes", RsNotes);
customElements.define("rs-note", RsNote);
customElements.define("rs-note-actions", RsNoteActions);
customElements.define("rs-note-new", RsNoteNew);
customElements.define("rs-account", RsAccount);

const ApplicationRouter = class extends HTMLElement {
  connectedCallback() {
    page.base(window.location.pathname);
    this.setupRoutes();
  }
  setupRoutes() {
    page("/", () => {
      this.innerHTML = "";
      this.removeAttribute("layout");
      let $navSite = document.createElement("nav-site");
      let $rsNotes = document.createElement("rs-notes");
      this.append($navSite);
      this.append($rsNotes);
      $navSite.querySelector("a").focus();
    });

    page("/note", (ctx, next) => {
      this.innerHTML = "";
      this.removeAttribute("layout");

      let $rsNoteNew = document.createElement("rs-note-new");
      this.append($rsNoteNew);
    });

    page("/note/:id", (ctx, next) => {
      this.innerHTML = "";
      this.removeAttribute("layout");

      const { id } = ctx.params;
      let $rsNote = document.createElement("rs-note");
      $rsNote.setAttribute("note-id", id);
      this.append($rsNote);
    });

    page("/account", () => {
      this.innerHTML = "";
      this.setAttribute("layout", "center");

      let $rsAccount = document.createElement("rs-account");
      this.append($rsAccount);
    });

    page();
  }
};

export default ApplicationRouter;
