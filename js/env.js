const isDevelopment = window.location.origin.includes('localhost:')

/* for gitlab pages hosting user.gitlab.io/i4k-notes */
/* const sitePathname =  isDevelopment ? '' : '/i4k-notes' */
const sitePathname =  isDevelopment ? '' : ''
export {
    isDevelopment,
    sitePathname
}

