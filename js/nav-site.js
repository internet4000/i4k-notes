import {buildLink} from './router.js'

const NavSite = class extends HTMLElement {
    constructor() {
	super()
    }

    connectedCallback() {
	this.render()
    }

    render() {
	let $nav = document.createElement('nav')
	let $navLinkNewNote = buildLink({
	    text: 'note',
	    href: '/note',
	    title: 'Create a new note (with a random ID)'
	})

	$nav.append($navLinkNewNote)
	this.append($nav)
    }
}

export default NavSite
