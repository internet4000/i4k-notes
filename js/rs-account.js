import NavAccount from './nav-account.js'

customElements.define('nav-account', NavAccount)

const RsAccount = class extends HTMLElement {
    connectedCallback() {
	this.render()
    }

    render() {
	let $navAccount = document.createElement('nav-account')
	let $rsWidget = document.createElement('rs-widget')
	this.append($navAccount)
	this.append($rsWidget)
	$rsWidget.remoteWidget.open()
	$rsWidget.remoteWidget.leaveOpen = true

	const $loginBtn = this.querySelector('button.rs-choose-rs')
	if ($loginBtn) {
	    $loginBtn.focus()
	} else {
	    this.querySelector('button.rs-sync').focus()
	}
    }
}

export default RsAccount
