import {notify} from './notifications.js'
import {page} from './router.js'

const RsNoteNew = class extends HTMLElement {
    constructor() {
	super()
    }

    async connectedCallback() {
	this.getAttributes()
	this.setupRS()
	const newNoteId =  this.remoteStorage.note4000.generateNoteId()
	await this.navigateToNote(newNoteId)

    }

    getAttributes() {
	// config
	this.debug = this.getAttribute('debug') ? true : false
	this.noteId = this.getAttribute('note-id') || null
    }

    setupRS() {
	this.remoteStorageDOM = document.querySelector('rs-storage')

	if (!this.remoteStorageDOM) {
	    console.error('Missing <rs-storage> element')
	    return
	}

	this.remoteStorage = this.remoteStorageDOM.remoteStorage
    }

    navigateToNote = (newNoteId) => {
	page.redirect(`/note/${newNoteId}`)
    }
}

export default RsNoteNew
