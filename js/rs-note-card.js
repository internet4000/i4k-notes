import {buildLink} from './router.js'
import {page} from './router.js'

const RsNoteCard = class extends HTMLElement {
    async connectedCallback() {
	this.getAttributes()
	this.render()
    }

    getAttributes() {
	// config
	this.debug = this.getAttribute('debug') ? true : false
	this.noteId = this.getAttribute('note-id') || null
	this.noteData = this.getAttribute('note-data') || null
    }

    handleDelete = () => {
	this.setAttribute('deleted', true)
	setTimeout(() => {
	    if (this && this.parentNode) {
		this.parentNode.removeChild(this)
	    }
	}, 300)
    }

    render() {
	const $noteActions = document.createElement('rs-note-actions')
	$noteActions.setAttribute('note-id', this.noteId)
	$noteActions.addEventListener('noteDeleted', this.handleDelete, false)
	
	const $noteLink = buildLink({
	    text: this.noteData || '',
	    href: `/note/${this.noteId}`,
	    title: `Go to note ${this.noteId} edit page`
	})

	this.appendChild($noteActions)
	this.appendChild($noteLink)
    }
}

export default RsNoteCard
