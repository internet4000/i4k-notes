Notes taking apps with remote storage js and custom elements HTML/JS web components.

This app takes the form of a html custom element, `<i4k-note></i4k-note>`, that can be embeded in any web page, when its javascript file is imported.

The user's data is stored in their [remotestorage](access-claim-directory), in the folder specified by the parameter `access-claim-directory` on the html element itself.

On first usage, a new user will have to authorize the domain access to the requested directory

The application instanciates *storagestorage* (in element `rs-storage`) with *access-claim-rights* `rw` (read & write) permissions. Find more about what the remote data module does in thefile `js/storage-modules/notes.js`.

# Usage

To instanciate the application you can do this (see `./index.html` for a practical example):
```
<i4k-notes
    debug="true"
    access-claim-directory="<your-app-remotestorage-directory>"
    api-key-dropbrox="<your-app-dropbox-api-key>"
    api-key-googledrive="<your-app-gdrive-api-key>"
></i4k-notes>
```

```
<script type="module">
	import './js/index.js'
</script>
```

Optional parameters are `debug`, `api-key-*`.

The parameter `access-claim-directory` specifies the *remotestorage* folder that will be used to save the data in the user remote-storage account (or Google Drive, or Dropbox if activated);

# Google Drive and Dropbox Keys

To get api keys for your instance of the application, [read the docs here](https://remotestoragejs.readthedocs.io/en/v1.2.3/getting-started/dropbox-and-google-drive.html).

# Development

Run dev server with:

```
serve -s .
```

If you ommit `-s`, it is without the single page server, and links won't work.


If you do not use the serve development server, you will get the error when opening the `index.html` file in your browser:
```
Cross-Origin Request Blocked: The Same Origin Policy disallows reading the remote resource at file:///[path-to-project]/js/index.js. (Reason: CORS request not http).
```

The javascript module seems to require beeing served.
